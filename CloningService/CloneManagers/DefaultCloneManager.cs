﻿using System;
using System.Collections;
using System.Reflection;
using Cloning.CloneContexts;

namespace Cloning.CloneManagers
{
    /// <summary>
    /// Default Clone Manager
    /// </summary>
    internal class DefaultCloneManager : ICloneManager
    {
        public virtual T Clone<T>(T source, ICloningContext context)
        {
            if (source == null) //Not initialized objects
                return source;

            if (context == null)
                throw new ArgumentNullException(nameof(context));

            Type sourceType = source.GetType();

            if (sourceType.IsValueType && sourceType.IsPrimitive)
            {
                return source;
            }
            else if (sourceType.IsValueType && !sourceType.IsPrimitive)
            {
                return CloneStruct(source, sourceType, context);
            }
            else if (sourceType.IsArray)
            {
                return CloneArray(source, sourceType, context);
            }
            else if (source is ICollection)
            {
                return CloneCollection(source, sourceType, context);
            }
            else
            {
                return CloneObject(source, sourceType, context);
            }
        }

        /// <summary>
        /// Clone some property or filed with CloningMode analysis
        /// </summary>
        /// <typeparam name="T">Type of member</typeparam>
        /// <param name="memberValue">Value</param>
        /// <param name="memberInfo">Info</param>
        /// <param name="parent">Parent instance</param>
        /// <returns>Cloned member value</returns>
        protected virtual T GetClonePropertyOrFieldValue<T>(T memberValue, MemberInfo memberInfo, object parent, object clonedParent,
            ICloningContext context)
        {
            //Null-value doesn't need processing
            if (memberValue == null)
                return memberValue;

            var attr = memberInfo?.GetCustomAttribute<CloneableAttribute>();
            var memberType = memberValue.GetType();

            //Check self-reference
            if (object.ReferenceEquals(memberValue, parent))
            {
                return (T)clonedParent;
            }
            //Shallow cloning
            else if (attr?.Mode == CloningMode.Shallow || memberType.IsPrimitive)
            {
                return memberValue;
            }
            //Deep cloning
            else if ((attr == null || attr.Mode == CloningMode.Deep) && !memberType.IsPrimitive)
            {
                return this.Clone(memberValue, context);
            }
            //Unexcepted cases
            else
            {
                throw new InvalidOperationException("Member can't be cloned");
            }
        }

        /// <summary>
        /// Clone members of object
        /// </summary>
        /// <typeparam name="T">Type of source</typeparam>
        /// <param name="source">Source instance</param>
        /// <param name="clone">Clone instance</param>
        /// <param name="sourceType">Source type</param>
        /// <exception cref="ArgumentNullException"></exception>
        protected virtual void ProcessingAllObjectMembers<T>(T source, T clone, Type sourceType, ICloningContext context)
        {
            /* 1. Processing all type members
             * 2. Take every member is property or field which is passed for cloning
             * 3. Set value member in the clone */
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            if (clone == null)
                throw new ArgumentNullException(nameof(clone));
            if (sourceType == null)
                throw new ArgumentNullException(nameof(sourceType));

            foreach (MemberInfo member in sourceType.GetMembers(BindingFlags.Public | BindingFlags.Instance))
            {
                if (member.MemberType == MemberTypes.Property)
                {
                    PropertyInfo propertyInfo = member as PropertyInfo;
                    if (propertyInfo.CanWrite &&
                        propertyInfo.CanRead &&
                        propertyInfo.GetCustomAttribute<CloneableAttribute>()?.Mode != CloningMode.Ignore)
                    {
                        propertyInfo.SetValue(clone,
                            this.GetClonePropertyOrFieldValue(propertyInfo.GetValue(source), member, source, clone, context));
                    }
                }
                if (member.MemberType == MemberTypes.Field)
                {
                    FieldInfo fieldInfo = member as FieldInfo;
                    if (fieldInfo.IsPublic &&
                        fieldInfo.GetCustomAttribute<CloneableAttribute>()?.Mode != CloningMode.Ignore)
                    {
                        fieldInfo.SetValue(clone,
                            this.GetClonePropertyOrFieldValue(fieldInfo.GetValue(source), member, source, clone, context));
                    }
                }
            }
        }

        /// <summary>
        /// Clone string
        /// </summary>
        /// <typeparam name="T">Type of string</typeparam>
        /// <param name="source">String instance</param>
        public virtual T CloneString<T>(T source)
        {
            if (source == null)
                return source;

            //Using standard string implementation
            return (T)(source as string).Clone();
        }

        /// <summary>
        /// Clone struct
        /// </summary>
        /// <typeparam name="T">Type of source</typeparam>
        /// <param name="source">Source instance</param>
        /// <param name="sourceType">Source type</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <returns>Cloned struct</returns>
        public virtual T CloneStruct<T>(T source, Type sourceType, ICloningContext context)
        {
            /* 1. Create instance of struct
             * 2. Processing all struct members
             * 3. Return upcasted instance */
            if (!sourceType.IsValueType)
                throw new ArgumentException("Source is not struct-type object", nameof(sourceType));
            if (sourceType == null)
                throw new ArgumentNullException(nameof(sourceType));

            object clone = Activator.CreateInstance(sourceType);

            ProcessingAllObjectMembers(source, clone, sourceType, context);

            return (T)clone;
        }

        /// <summary>
        /// Clone refernce-type object
        /// </summary>
        /// <typeparam name="T">Type of source</typeparam>
        /// <param name="source">Source instance</param>
        /// <param name="sourceType">Source type</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>Cloned collection</returns>
        public virtual T CloneObject<T>(T source, Type sourceType, ICloningContext context)
        {
            /* 1. Check to exists clone for this source in the context => success => return clone from the context
             * 2. Check to this object is string => use standard method
             * 3. Get X() constructor
             * 4. Create clone by him
             * 5. Processing all mem
             * 6. Push self into the context
             * 7. Return upcasted instance */

            if (source == null)
                return source;
            if (sourceType == null)
                throw new ArgumentNullException(nameof(sourceType));

            object clone = context.Pull(source);
            if (clone != null)
                return (T)clone;

            if (source is string)
                return CloneString(source);

            var constructor = sourceType.GetConstructor(new Type[] { });
            if (constructor == null)
                throw new InvalidOperationException("The source type has not constructors without parameters");

            clone = constructor.Invoke(null);
            context.Push(source, clone);

            ProcessingAllObjectMembers(source, clone, sourceType, context);

            return (T)clone;
        }

        /// <summary>
        /// Clone array
        /// </summary>
        /// <typeparam name="T">Type of source array</typeparam>
        /// <param name="source">Array instance</param>
        /// <param name="sourceType">Array type</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>Cloned array</returns>
        public virtual T CloneArray<T>(T source, Type sourceType, ICloningContext context)
        {
            /* 1. Check to exists clone for this source in the context => success => return clone from the context
             * 2. Create array instance
             * 3. Clone all array elements
             * 4. Return upcasted instance */
            if (source == null)
                return source;
            if (!(source is Array))
                throw new ArgumentException("Source is not array", nameof(source));
            if (sourceType == null)
                throw new ArgumentNullException(nameof(sourceType));

            object clone = context.Pull(source);
            if (clone != null)
                return (T)clone;

            clone = Array.CreateInstance(sourceType.GetElementType(), (source as Array).Length);
            Array cloneRef = clone as Array;

            context.Push(source, clone);

            int i = 0;
            foreach (object element in (source as IEnumerable))
            {
                object clonedItem = context.Pull(element);
                if (clonedItem == null)
                    clonedItem = this.Clone(element, context);

                cloneRef.SetValue(clonedItem, i);
                i++;
            }

            return (T)clone;
        }

        /// <summary>
        /// Clone collection which implemented ICollection interface
        /// </summary>
        /// <typeparam name="T">Type of source collection</typeparam>
        /// <param name="source">Collection instance</param>
        /// <param name="sourceType">Collection type</param>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns>Cloned collection</returns>
        public virtual T CloneCollection<T>(T source, Type sourceType, ICloningContext context)
        {
            /* 1. Check to exists clone for this source in the context => success => return clone from the context
             * 2. Get X() constructor of collection
             * 3. Create clone by him
             * 4. Sequential cloning all elements
             * 5. Return upcasted instance */

            if (source == null)
                return source;
            if (!(source is ICollection))
                throw new ArgumentException("Source is not ICollection compatible", nameof(source));
            if (sourceType == null)
                throw new ArgumentNullException(nameof(sourceType));

            object clone = context.Pull(source);
            if (clone != null)
                return (T)clone;

            var constructor = sourceType.GetConstructor(new Type[] { });
            if (constructor == null)
                throw new InvalidOperationException("The source type has not constructors without parameters");
            clone = constructor.Invoke(null);

            context.Push(source, clone);

            //Get reference for 'Add' method, so that not use upcasting to ICollection<> interface
            var cloneAddMethodRef = clone?.GetType().GetMethod("Add", BindingFlags.Public | BindingFlags.Instance);
            if (cloneAddMethodRef == null)
                throw new InvalidOperationException("The source type has not contain 'Add' method");

            int i = 0;
            foreach (object element in (source as IEnumerable))
            {
                object clonedIntem = context.Pull(element);
                if (clonedIntem == null)
                    clonedIntem = this.Clone(element, context);

                cloneAddMethodRef.Invoke(clone, new object[] { clonedIntem });
                i++;
            }

            return (T)clone;
        }
    }
}
