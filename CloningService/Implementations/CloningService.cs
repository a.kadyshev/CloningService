﻿using Cloning.CloneContexts;
using Cloning.CloneManagers;

namespace Cloning
{
    /// <summary>
    /// Service of cloning
    /// </summary>
    public class CloningService : ICloningService
    {
        private ICloneManager serviceCloneManager;

        /// <summary>
        /// Clone the 'source'
        /// </summary>
        /// <typeparam name="T">Type of the source object (any type)</typeparam>
        /// <param name="source">Instance of the source object</param>
        /// <returns>Clone of source object</returns>
        public T Clone<T>(T source)
        {
            if (source == null)
                return source;

            //Use common cloning context for one unit of work allows avoid looping
            using (ICloningContext context = new DefaultCloningContext())
            {
                return serviceCloneManager.Clone(source, context);
            }
        }

        /// <summary>
        /// Initialize Cloning Service with default clone manager
        /// </summary>
        public CloningService() => serviceCloneManager = new DefaultCloneManager();

        /// <summary>
        /// Initialize Cloning Service with custom clone manager
        /// </summary>
        /// <param name="cloneManager">Clone manager instance</param>
        internal CloningService(ICloneManager cloneManager) => serviceCloneManager = cloneManager;
    }
}
