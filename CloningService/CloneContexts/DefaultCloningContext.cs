﻿using System.Collections;

namespace Cloning.CloneContexts
{
    /// <summary>
    /// Context of the cloning process for one object
    /// </summary>
    /// <remarks>It is used for avoiding repeatable clonings</remarks>
    internal class DefaultCloningContext : ICloningContext
    {
        private Hashtable _objectsTable;

        /// <summary>
        /// Initialize cloning context
        /// </summary>
        /// <param name="elementsCount">Elements in context</param>
        public DefaultCloningContext(int elementsCount = 1)
        {
            _objectsTable = new Hashtable(elementsCount, new CloningEqualityComparer());
        }

        public void Push(object source, object cloned)
        {
            _objectsTable.Add(source, cloned);
        }

        public object Pull(object element)
        {
            return _objectsTable[element]; ;
        }


        public void Dispose()
        {
            _objectsTable.Clear();
        }
    }
}
