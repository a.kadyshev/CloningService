# CloningService

The ClonnigService is a simple service for cloning instances of objects.

It can clone:
- Value-type objects (primitive, structs)
- Reference-type objects
- Arrays
- ICollecion-based collections

Features:
- Only public fields and properties can be cloned
- Supports 'deep' or 'shallow' cloning
- Allow ignoring some properties or fields  
- There is a choice of cloning mode by attributes (deep, shallow, ignore)
- Keeping reference-hierarchy

Example of use:

...
```
public class ExampleClass
{
            public int I;

            public string S { get; set; }

            [Cloneable(CloningMode.Ignore)]
            public string Ignored { get; set; }

            [Cloneable(CloningMode.Shallow)]
            public object Shallow { get; set; }
}
```
...
```
ICloningService Cloner = new CloningService();
var s = new ExampleClass() { I = 1, S = "2", Ignored = "3", Shallow = new object() };
var c = Cloner.Clone(s);
```

...