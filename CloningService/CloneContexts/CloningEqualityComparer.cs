﻿using System;
using System.Collections;

namespace Cloning.CloneContexts
{
    /// <summary>
    /// Comparer of object references  
    /// </summary>
    internal class CloningEqualityComparer : IEqualityComparer
    {
        bool IEqualityComparer.Equals(object x, object y)
        {
            return Object.ReferenceEquals(x, y);
        }

        int IEqualityComparer.GetHashCode(object obj)
        {
            return obj.GetHashCode();
        }
    }
}
