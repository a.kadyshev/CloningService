﻿using System;

namespace Cloning.CloneContexts
{
    /// <summary>
    /// Interface for Cloninig Contexts
    /// </summary>
    internal interface ICloningContext : IDisposable
    {
        /// <summary>
        /// Push into context Source-Clone pair
        /// </summary>
        /// <param name="source">Source object</param>
        /// <param name="cloned">Clone object</param>
        void Push(object source, object cloned);

        /// <summary>
        /// Pull Clone-object form context
        /// </summary>
        /// <param name="element">Source object</param>
        /// <returns>Clone reference</returns>
        object Pull(object element);
    }
}
