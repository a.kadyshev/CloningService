﻿namespace Cloning
{
    /// <summary>
    /// Interface for Cloning Services
    /// </summary>
    public interface ICloningService
    {
        T Clone<T>(T source);
    }
}
