﻿using System;

namespace Cloning
{
    /// <summary>
    /// Cloning modes
    /// </summary>
    public enum CloningMode
    {
        /// <summary>
        /// Copy object with content
        /// </summary>
        Deep = 0,

        /// <summary>
        /// Copy reference
        /// </summary>
        Shallow = 1,

        /// <summary>
        /// Ignore member
        /// </summary>
        Ignore = 2,
    }

    /// <summary>
    /// Clonable attribute - sets cloning mode for class member
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class CloneableAttribute : Attribute
    {
        /// <summary>
        /// Cloning mode
        /// </summary>
        public CloningMode Mode { get; }

        /// <summary>
        /// Initialize
        /// </summary>
        /// <param name="mode">Cloning mode</param>
        public CloneableAttribute(CloningMode mode)
        {
            Mode = mode;
        }
    }
}
