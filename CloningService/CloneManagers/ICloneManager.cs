﻿using System;
using Cloning.CloneContexts;

namespace Cloning.CloneManagers
{
    /// <summary>
    /// Interface for Clone Managers
    /// </summary>
    internal interface ICloneManager
    {
        /// <summary>
        /// Clone any object
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="source">Instance</param>
        /// <param name="context">Cloning context</param>
        /// <returns>Cloned instance</returns>
        T Clone<T>(T source, ICloningContext context);

        /// <summary>
        /// Clone structure
        /// </summary>
        /// <typeparam name="T">Struct-based type</typeparam>
        /// <param name="source">Instance</param>
        /// <param name="sourceType">Type of source</param>
        /// <param name="context">Cloning context</param>
        /// <returns></returns>
        T CloneStruct<T>(T source, Type sourceType, ICloningContext context);

        /// <summary>
        /// Clone outer referece-type-based object
        /// </summary>
        /// <typeparam name="T">Object-based reference-type object (not collection or array)</typeparam>
        /// <param name="source">Instance</param>
        /// <param name="sourceType">Type of source</param>
        /// <param name="context">Cloning context</param>
        /// <returns></returns>
        T CloneObject<T>(T source, Type sourceType, ICloningContext context);

        /// <summary>
        /// Clone array
        /// </summary>
        /// <typeparam name="T">Array-based type</typeparam>
        /// <param name="source">Instance</param>
        /// <param name="sourceType">Type of source</param>
        /// <param name="context"></param>
        /// <returns>Cloned array</returns>
        T CloneArray<T>(T source, Type sourceType, ICloningContext context);

        /// <summary>
        /// Clone collection
        /// </summary>
        /// <typeparam name="T">ICollection-based type</typeparam>
        /// <param name="source">Instance</param>
        /// <param name="sourceType">Type of source</param>
        /// <param name="context">Cloning context</param>
        /// <returns>Cloned collection</returns>
        T CloneCollection<T>(T source, Type sourceType, ICloningContext context);
    }
}
